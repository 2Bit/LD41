// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainAIController.h"

#include "MainCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"

AMainAIController::AMainAIController()
{
	this->CurrentMotive = EAIMotive::EngageFar;
}

void AMainAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	AMainCharacter* const MainCharacter = Cast<AMainCharacter>(this->GetPawn());
	if (MainCharacter != nullptr)
	{
		AMainCharacter* const Target = Cast<AMainCharacter>(MainCharacter->Target);
		if (Target != nullptr)
		{
			this->SetFocalPoint(Target->GetActorLocation());

			FAIPayload AIPayload;
			AIPayload.Self = MainCharacter;
			AIPayload.Target = Target;
			AIPayload.DeltaSeconds = DeltaSeconds;
			AIPayload.DistanceToTarget = (MainCharacter->Target->GetActorLocation() - MainCharacter->GetActorLocation()).Size2D();
			AIPayload.IsInMeleeRange = AIPayload.DistanceToTarget <= 150.0f;
			AIPayload.IsAtFarRange = AIPayload.DistanceToTarget >= 500.0f;

			this->CalculateMotive(AIPayload);

			if (AIPayload.Self->IsStaggered())
			{
				AIPayload.Self->Block();
			}
			else
			{
				switch (this->CurrentMotive)
				{
				case EAIMotive::EngageFar:
					if (AIPayload.Self->HasEnergy(0.5f))
					{
						if (AIPayload.IsAtFarRange)
						{
							if (this->CurrentCombo.Num() <= 0)
							{
								this->CalculateComboAttack(AIPayload, EAttackType::Ranged);
							}
						}
						else
						{
							AIPayload.Self->MoveForward(-1.0f);
							this->CurrentCombo.Empty();
						}

						break;
					}
				case EAIMotive::EngageClose:
					if (AIPayload.IsInMeleeRange)
					{
						if (this->CurrentCombo.Num() <= 0)
						{
							if (FMath::FRand() < 0.5f)
							{
								this->CalculateComboAttack(AIPayload, EAttackType::Light);
							}
							else
							{
								this->CalculateComboAttack(AIPayload, EAttackType::Heavy);
							}
						}
					}
					else
					{
						AIPayload.Self->MoveForward(1.0f);
						this->CurrentCombo.Empty();
					}

					break;
				}

				if (!AIPayload.Self->IsAttacking() && this->CurrentCombo.Num() > 0)
				{
					AIPayload.Self->PerformAttack(this->CurrentCombo[0]);
					this->CurrentCombo.RemoveAt(0);
				}

				TArray<FOverlapResult> Overlaps;
				this->GetWorld()->OverlapMultiByChannel(
					Overlaps,
					AIPayload.Self->GetActorLocation() + AIPayload.Self->GetActorForwardVector() * 300.0f,
					FQuat::Identity,
					ECollisionChannel::ECC_WorldDynamic,
					FCollisionShape::MakeCapsule(300.0f, 100.0f));
				for (int i = 0; i < Overlaps.Num(); ++i)
				{
					UProjectileMovementComponent* const ProjectileOverlap = Overlaps[i].GetActor()->FindComponentByClass<UProjectileMovementComponent>();
					if (ProjectileOverlap != nullptr && ProjectileOverlap->GetOwner()->GetInstigator() != AIPayload.Self)
					{
						if (FMath::FRand() < 0.5f)
						{
							AIPayload.Self->DodgeLeft();
						}
						else
						{
							AIPayload.Self->DodgeRight();
						}
					}
				}
			}
		}
	}
}

void AMainAIController::CalculateMotive(FAIPayload const& AIPayload)
{
	float static const MOTIVE_CHANGE_INTERVAL_MINIMUM = 2.0f;
	float static const MOTIVE_CHANGE_INTERVAL_MAXIMUM = 6.0f;

	float const MotiveChangeChance = 
		FMath::Clamp(
			((this->GetWorld()->GetTimeSeconds()
				- (this->LastMotiveChangeTime + MOTIVE_CHANGE_INTERVAL_MINIMUM))
			/ MOTIVE_CHANGE_INTERVAL_MAXIMUM),
			0.0f,
			1.0f);

	if (MotiveChangeChance > 0.0f && FMath::FRand() <= MotiveChangeChance * AIPayload.DeltaSeconds)
	{
		if (AIPayload.IsAtFarRange)
		{
			if (FMath::FRand() <= 0.7f)
			{
				this->CurrentMotive = EAIMotive::EngageFar;
			}
			else
			{
				this->CurrentMotive = EAIMotive::EngageClose;
			}
		}
		else
		{
			if (FMath::FRand() <= 0.5f)
			{
				this->CurrentMotive = EAIMotive::EngageClose;
			}
			else
			{
				this->CurrentMotive = EAIMotive::EngageFar;
			}
		}

		this->LastMotiveChangeTime = this->GetWorld()->GetTimeSeconds();
	}
}

void AMainAIController::CalculateComboAttack(FAIPayload const& AIPayload, EAttackType Attack)
{
	this->CurrentCombo.Empty();

	int PossibleCombos = 0;
	for (int i = 0; i < AIPayload.Self->Combos.Num(); ++i)
	{
		if (AIPayload.Self->Combos[i].Triggers[AIPayload.Self->Combos[i].Triggers.Num() - 1] == Attack)
		{
			++PossibleCombos;
		}
	}

	if (PossibleCombos > 0)
	{
		int SkippedCombos = 0;
		for (int i = 0; i < AIPayload.Self->Combos.Num(); ++i)
		{
			if (AIPayload.Self->Combos[i].Triggers[AIPayload.Self->Combos[i].Triggers.Num() - 1] == Attack)
			{
				if (SkippedCombos >= PossibleCombos - 1 || FMath::FRand() <= 1.0f / PossibleCombos)
				{
					for (int k = 0; k < AIPayload.Self->Combos[i].Triggers.Num(); ++k)
					{
						this->CurrentCombo.Add(AIPayload.Self->Combos[i].Triggers[k]);
					}

					break;
				}
				else
				{
					++SkippedCombos;
				}
			}
		}
	}
	else
	{
		this->CurrentCombo.Add(Attack);
	}
}
