// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "MainCharacter.generated.h"

UENUM(BlueprintType)
enum class EAttackType : uint8
{
	Light = 0,
	Heavy = 1,
	Ranged = 2,
};

USTRUCT(BlueprintType)
struct FAttackCombo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<EAttackType> Triggers;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class UAnimMontage* Animation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName Section;
};

UCLASS(config=Game)
class AMainCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY()
	class UAnimMontage* CurrentAttack;

	UPROPERTY()
	FVector CurrentAttackLocation;

	UPROPERTY()
	float CurrentAttackEndTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* LightAttackAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* HeavyAttackAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* RangedAttackAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* DodgeAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* RecoilAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* BlockAnims;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimMontage* CounterAnims;

	UPROPERTY()
	TArray<EAttackType> CurrentCombo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	FString Name;

	UPROPERTY(EditDefaultsOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float CurrentHealth;

	UPROPERTY()
	float MaximumHealth;

	UPROPERTY()
	bool IsStationary;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float Energy;

	UPROPERTY()
	float LastDodgeTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects, meta = (AllowPrivateAccess = "true"))
	class UParticleSystem* LaunchEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects, meta = (AllowPrivateAccess = "true"))
	class UParticleSystem* HitEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	TArray<class USoundCue*> HitSounds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	TArray<class USoundCue*> LaunchSounds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	TArray<class USoundCue*> CounterSounds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	TArray<class USoundCue*> DodgeSounds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AActor> ProjectileClass;

public:
	AMainCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	AActor* Target;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	TArray<FAttackCombo> Combos;

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

protected:
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:
	UFUNCTION(BlueprintCallable)
	bool DealDamage(float Damage, FName OriginBone, FVector LaunchVelocity);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurrentHealthPercent() const;

	UFUNCTION(BlueprintCallable)
	void DrainEnergy(float DrainAmount);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool HasEnergy(float Amount) const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsAttacking() const;

	UFUNCTION(BlueprintCallable)
	void PerformAttack(EAttackType AttackType);

	void PerformLightAttack();
	void PerformHeavyAttack();
	void PerformRangedAttack();

	UFUNCTION(BlueprintCallable)
	void StopAttacking();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsBlocking() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool CanBlock() const;

	UFUNCTION(BlueprintCallable)
	void Block();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsCountering() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsStaggered() const;

	void MoveForward(float Value);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsDodging() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool CanDodge() const;

	void DodgeRight();
	void DodgeLeft();

	void Pause();

	UFUNCTION(BlueprintCallable)
	void PlaySoundEffect(TArray<class USoundCue*> PotentialSounds, FVector Location);

public:
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return this->CameraBoom; }
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return this->FollowCamera; }
	FORCEINLINE float GetEnergy() const { return this->Energy; }
};
