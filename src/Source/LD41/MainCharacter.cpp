// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainCharacter.h"

#include "MainAIController.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Animation/AnimInstance.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

static float VOLUME = 0.4f;

AMainCharacter::AMainCharacter()
{
	this->GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	this->bUseControllerRotationPitch = false;
	this->bUseControllerRotationYaw = true;
	this->bUseControllerRotationRoll = false;

	this->GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	this->GetCharacterMovement()->JumpZVelocity = 600.f;
	this->GetCharacterMovement()->AirControl = 0.2f;

	this->CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	this->CameraBoom->SetupAttachment(this->RootComponent);
	this->CameraBoom->RelativeRotation = FRotator(-15.0f, 0.0f, 0.0f);
	this->CameraBoom->SocketOffset = FVector(0.0f, 50.0f, 0.0f);
	this->CameraBoom->TargetArmLength = 300.0f;
	this->CameraBoom->bUsePawnControlRotation = true;
	this->CameraBoom->bInheritPitch = false;

	this->FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	this->FollowCamera->SetupAttachment(this->CameraBoom, USpringArmComponent::SocketName);
	this->FollowCamera->bUsePawnControlRotation = false;

	this->PrimaryActorTick.bCanEverTick = true;

	this->AIControllerClass = AMainAIController::StaticClass();

	this->CurrentHealth = 100.0f;
}

void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	this->MaximumHealth = this->CurrentHealth;
}

void AMainCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (this->GetController() != nullptr && this->Target != nullptr)
	{
		FVector const TargetDisplacement = this->Target->GetActorLocation() - this->GetActorLocation();
		float const TargetViewWeight = FMath::Min(1.0f, 300.0f / (TargetDisplacement.Size() + 300.0f));

		FRotator NewRotation = this->GetController()->GetControlRotation();
		NewRotation.Yaw = TargetDisplacement.ToOrientationRotator().Yaw;
		this->GetController()->SetControlRotation(NewRotation);

		this->CameraBoom->SocketOffset = FVector(0.0f, 50.0f + 300.0f * TargetViewWeight, 0.0f);
		this->FollowCamera->SetRelativeRotation(FRotator(0.0f, -45.0f * TargetViewWeight, 0.0f));
	}

	if (this->GetVelocity().SizeSquared() < 400.0f * 400.0f)
	{
		this->IsStationary = true;
	}
	else if (this->IsStationary)
	{
		UGameplayStatics::SpawnEmitterAtLocation(
			this->GetWorld(),
			this->LaunchEffect,
			this->GetMesh()->GetComponentLocation());

		this->IsStationary = false;
	}

	if (!this->IsAttacking() && this->GetWorld()->GetTimeSeconds() > this->CurrentAttackEndTime + 0.2f)
	{
		this->StopAttacking();
	}

	this->DrainEnergy(-DeltaSeconds / 5);

	if (this->GetActorRotation().UnrotateVector(this->GetLastMovementInputVector()).X < 0.0f)
	{
		this->GetCharacterMovement()->MaxWalkSpeed = 400.0f;
	}
	else
	{
		this->GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	}
}

void AMainCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Light Attack", IE_Pressed, this, &AMainCharacter::PerformLightAttack);
	PlayerInputComponent->BindAction("Heavy Attack", IE_Pressed, this, &AMainCharacter::PerformHeavyAttack);
	PlayerInputComponent->BindAction("Ranged Attack", IE_Pressed, this, &AMainCharacter::PerformRangedAttack);

	PlayerInputComponent->BindAction("Block", IE_Pressed, this, &AMainCharacter::Block);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);

	PlayerInputComponent->BindAction("Dodge Right", IE_Pressed, this, &AMainCharacter::DodgeRight);
	PlayerInputComponent->BindAction("Dodge Left", IE_Pressed, this, &AMainCharacter::DodgeLeft);

	FInputActionBinding& Binding = PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &AMainCharacter::Pause);
	Binding.bExecuteWhenPaused = true;
}

float AMainCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (!this->IsDodging() && !this->IsCountering())
	{
		if (this->IsBlocking())
		{
			this->DrainEnergy(1.0f);

			this->PlayAnimMontage(this->CounterAnims);
		}
		else if (!this->GetMesh()->GetAnimInstance()->Montage_IsPlaying(this->BlockAnims))
		{
			this->CurrentHealth -= Damage;

			if (this->CurrentHealth <= 0.0f)
			{
				this->Destroy();
			}

			float const EmitterScale = Damage / 2.0f;

			if (DamageEvent.IsOfType(1))
			{
				FPointDamageEvent const* PointDamageEvent = static_cast<const FPointDamageEvent*>(&DamageEvent);

				float const HitHeight = (PointDamageEvent->HitInfo.ImpactPoint - this->GetActorLocation()).Z;
				if (HitHeight < -10.0f)
				{
					this->PlayAnimMontage(this->RecoilAnims, 1.0f, FName("Low"));
				}
				else if (HitHeight < 20.0f)
				{
					this->PlayAnimMontage(this->RecoilAnims, 1.0f, FName("Mid"));
				}
				else
				{
					this->PlayAnimMontage(this->RecoilAnims, 1.0f, FName("High"));
				}

				if (PointDamageEvent->ShotDirection.SizeSquared() < 1000.0f * 1000.0f)
				{
					this->PlaySoundEffect(this->HitSounds, PointDamageEvent->HitInfo.Location);
				}
				else
				{
					this->LaunchCharacter(this->GetActorRotation().RotateVector(PointDamageEvent->ShotDirection), false, false);

					this->PlaySoundEffect(this->LaunchSounds, PointDamageEvent->HitInfo.Location);
				}

				UGameplayStatics::SpawnEmitterAtLocation(
					this->GetWorld(),
					this->HitEffect,
					PointDamageEvent->HitInfo.Location)->SetWorldScale3D(FVector(EmitterScale, EmitterScale, EmitterScale));
			}
			else
			{
				this->PlayAnimMontage(this->RecoilAnims, 1.0f, FName("Mid"));

				UGameplayStatics::SpawnEmitterAtLocation(
					this->GetWorld(),
					this->HitEffect,
					this->GetActorLocation())->SetWorldScale3D(FVector(EmitterScale, EmitterScale, EmitterScale));
			}

			return Damage;
		}
	}
	else
	{
		this->PlaySoundEffect(this->CounterSounds, this->GetActorLocation());
	}

	return 0.0f;
}

bool AMainCharacter::DealDamage(float Damage, FName OriginBone, FVector LaunchVelocity)
{
	FVector const BoneLocation = this->GetMesh()->GetBoneLocation(OriginBone);

	TArray<FHitResult> Hits;
	if (this->GetWorld()->SweepMultiByChannel(Hits, BoneLocation, BoneLocation, FQuat::Identity, ECollisionChannel::ECC_Pawn, FCollisionShape::MakeCapsule(40.0f, 80.0f)))
	{
		for (int i = 0; i < Hits.Num(); ++i)
		{
			if (Hits[i].GetActor() != this && Hits[i].GetActor() != nullptr && !Hits[i].GetActor()->IsActorBeingDestroyed())
			{
				FPointDamageEvent DamageEvent;
				DamageEvent.Damage = 0.0f;
				DamageEvent.HitInfo = Hits[i];
				DamageEvent.ShotDirection = LaunchVelocity;
				Hits[i].GetActor()->TakeDamage(Damage, DamageEvent, this->GetInstigatorController(), this);
			}
		}
	}

	return false;
}

float AMainCharacter::GetCurrentHealthPercent() const
{
	if (this->CurrentHealth <= 0.0f)
	{
		return 0.0f;
	}

	return this->CurrentHealth / this->MaximumHealth;
}

void AMainCharacter::DrainEnergy(float DrainAmount)
{
	this->Energy = FMath::Clamp(this->Energy - DrainAmount, 0.0f, 1.0f);
}

bool AMainCharacter::HasEnergy(float Amount) const
{
	return this->Energy >= Amount;
}

bool AMainCharacter::IsAttacking() const
{
	return this->CurrentAttack != nullptr && this->GetMesh()->GetAnimInstance()->Montage_IsPlaying(this->CurrentAttack);
}

void AMainCharacter::PerformAttack(EAttackType AttackType)
{
	if (!this->IsStaggered() && !this->IsCountering() && !this->IsBlocking())
	{
		if (!this->IsAttacking())
		{
			this->CurrentAttackLocation = this->GetActorLocation();

			this->CurrentCombo.Add(AttackType);

			for (int i = 0; i < this->Combos.Num(); ++i)
			{
				if (this->CurrentCombo.Num() == this->Combos[i].Triggers.Num())
				{
					bool IsMatch = true;

					for (int k = 0; k < this->Combos[i].Triggers.Num(); ++k)
					{
						if (this->CurrentCombo[k] != this->Combos[i].Triggers[k])
						{
							IsMatch = false;

							break;
						}
					}

					if (IsMatch)
					{
						this->PlayAnimMontage(this->CurrentAttack = this->Combos[i].Animation, 1.0f, this->Combos[i].Section);
						this->CurrentAttackEndTime =
							this->GetWorld()->GetTimeSeconds()
							+ this->Combos[i].Animation->GetSectionTimeLeftFromPos(
								this->GetMesh()->GetAnimInstance()->Montage_GetPosition(this->Combos[i].Animation));

						this->CurrentCombo.Empty();

						return;
					}
				}
			}

			if (AttackType == EAttackType::Ranged)
			{
				if (this->HasEnergy(0.3f))
				{
					this->DrainEnergy(0.3f);
				}
				else
				{
					return;
				}
			}

			UAnimMontage* AttackAnims = nullptr;

			switch (AttackType)
			{
			case EAttackType::Light:
				AttackAnims = this->LightAttackAnims;

				break;
			case EAttackType::Heavy:
				AttackAnims = this->HeavyAttackAnims;

				break;
			case EAttackType::Ranged:
				AttackAnims = this->RangedAttackAnims;

				break;
			}

			if (AttackAnims != nullptr)
			{
				if (!AttackAnims->IsValidSectionIndex(this->CurrentCombo.Num() - 1))
				{
					this->CurrentCombo.Empty();
					this->CurrentCombo.Add(AttackType);
				}

				this->PlayAnimMontage(this->CurrentAttack = AttackAnims, 1.0f, AttackAnims->GetSectionName(this->CurrentCombo.Num() - 1));
				this->CurrentAttackEndTime =
					this->GetWorld()->GetTimeSeconds()
					+ AttackAnims->GetSectionTimeLeftFromPos(
						this->GetMesh()->GetAnimInstance()->Montage_GetPosition(AttackAnims));
			}
		}
	}
}

void AMainCharacter::PerformLightAttack()
{
	this->PerformAttack(EAttackType::Light);
}

void AMainCharacter::PerformHeavyAttack()
{
	this->PerformAttack(EAttackType::Heavy);
}

void AMainCharacter::PerformRangedAttack()
{
	this->PerformAttack(EAttackType::Ranged);
}

void AMainCharacter::StopAttacking()
{
	this->CurrentCombo.Empty();

	if (this->CurrentAttack != nullptr)
	{
		this->StopAnimMontage(this->CurrentAttack);

		this->CurrentAttack = nullptr;
	}
}

bool AMainCharacter::IsBlocking() const
{
	return this->GetMesh()->GetAnimInstance()->Montage_IsPlaying(this->BlockAnims);
}

bool AMainCharacter::CanBlock() const
{
	return this->HasEnergy(1.0f) && !this->IsDodging();
}

void AMainCharacter::Block()
{
	if (this->CanBlock())
	{
		this->PlayAnimMontage(this->BlockAnims);
	}
}

bool AMainCharacter::IsCountering() const
{
	return this->GetMesh()->GetAnimInstance()->Montage_IsPlaying(this->CounterAnims);
}

bool AMainCharacter::IsStaggered() const
{
	return this->GetMesh()->GetAnimInstance()->Montage_IsPlaying(this->RecoilAnims);
}

void AMainCharacter::MoveForward(float Value)
{
	if (!this->IsStaggered() && !this->IsCountering() && (this->GetController() != nullptr) && (Value != 0.0f))
	{
		FRotator const Rotation = this->GetController()->GetControlRotation();
		FRotator const YawRotation(0, Rotation.Yaw, 0);

		FVector const Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		this->AddMovementInput(Direction, Value);
	}
}

bool AMainCharacter::IsDodging() const
{
	return this->GetMesh()->GetAnimInstance()->Montage_IsPlaying(this->DodgeAnims);
}

bool AMainCharacter::CanDodge() const
{
	return !this->IsStaggered() && !this->IsCountering() && !this->IsDodging() && this->GetWorld()->GetTimeSeconds() >= this->LastDodgeTime + 0.2f;
}

void AMainCharacter::DodgeRight()
{
	if (this->CanDodge())
	{
		this->LaunchCharacter(this->GetActorRotation().RotateVector(FVector(0.0f, 1000.0f, 100.0f)), false, false);

		this->PlayAnimMontage(this->DodgeAnims, 1.0f, FName("Right"));

		if (!this->IsStationary)
		{
			UGameplayStatics::SpawnEmitterAtLocation(
				this->GetWorld(),
				this->LaunchEffect,
				this->GetMesh()->GetBoneTransform(this->GetMesh()->GetBoneIndex(FName("Character1_LeftFoot"))));
		}

		this->PlaySoundEffect(this->DodgeSounds, this->GetActorLocation());

		this->LastDodgeTime = this->GetWorld()->GetTimeSeconds();
	}
}

void AMainCharacter::DodgeLeft()
{
	if (this->CanDodge())
	{
		this->LaunchCharacter(this->GetActorRotation().RotateVector(FVector(0.0f, -1000.0f, 100.0f)), false, false);

		this->PlayAnimMontage(this->DodgeAnims, 1.0f, FName("Left"));

		if (!this->IsStationary)
		{
			UGameplayStatics::SpawnEmitterAtLocation(
				this->GetWorld(),
				this->LaunchEffect,
				this->GetMesh()->GetBoneTransform(this->GetMesh()->GetBoneIndex(FName("Character1_RightFoot"))));
		}

		this->PlaySoundEffect(this->DodgeSounds, this->GetActorLocation());

		this->LastDodgeTime = this->GetWorld()->GetTimeSeconds();
	}
}

void AMainCharacter::Pause()
{
	APlayerController* const PlayerController = Cast<APlayerController>(this->Controller);
	if (PlayerController != nullptr)
	{
		PlayerController->SetPause(!PlayerController->IsPaused());
	}
}

void AMainCharacter::PlaySoundEffect(TArray<class USoundCue*> PotentialSounds, FVector Location)
{
	UGameplayStatics::PlaySoundAtLocation(
		this,
		PotentialSounds[FMath::RandRange(0, PotentialSounds.Num() - 1)],
		Location,
		(FMath::FRand() * 0.3f + 0.8f) * VOLUME,
		FMath::FRand() * 0.2f + 0.9f);
}
