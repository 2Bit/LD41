// Copyright 2018 2Bit Studios, All Rights Reserved.

#include "MainGameMode.h"

#include "MainCharacter.h"

AMainGameMode::AMainGameMode()
{
	this->DefaultPawnClass = AMainCharacter::StaticClass();
}
