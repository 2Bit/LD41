// Copyright 2018 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "MainCharacter.h"

#include "MainAIController.generated.h"

UENUM(BlueprintType)
enum class EAIMotive : uint8
{
	EngageClose = 0,
	EngageFar = 1,
};

USTRUCT()
struct FAIPayload
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	AMainCharacter* Self;

	UPROPERTY()
	AMainCharacter* Target;
	
	UPROPERTY()
	float DeltaSeconds;

	UPROPERTY()
	float DistanceToTarget;

	UPROPERTY()
	bool IsInMeleeRange;

	UPROPERTY()
	bool IsAtFarRange;
};

UCLASS()
class LD41_API AMainAIController : public AAIController
{
	GENERATED_BODY()

	UPROPERTY()
	EAIMotive CurrentMotive;

	UPROPERTY()
	float LastMotiveChangeTime;

	UPROPERTY()
	TArray<EAttackType> CurrentCombo;

public:
	AMainAIController();

	virtual void Tick(float DeltaSeconds) override;

	void CalculateMotive(FAIPayload const& AIPayload);
	void CalculateComboAttack(FAIPayload const& AIPayload, EAttackType Attack);
};
